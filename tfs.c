/*
 *  Copyright (C) 2020 CS416 Rutgers CS
 *	Tiny File System
 *	File:	tfs.c
 *
 */


#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/time.h>
#include <libgen.h>
#include <limits.h>
#include <math.h>

#include "block.h"
#include "tfs.h"

#define INODE_SIZE sizeof(struct inode)
#define NUM_INODES BLOCK_SIZE/INODE_SIZE
#define NUM_IBLKS MAX_INUM/NUM_INODES

#define DIRENT_SIZE sizeof(struct dirent)
#define NUM_DIRENTS (BLOCK_SIZE/DIRENT_SIZE)
#define ROOT "/"
#define CUR_DIR "."
#define PAR_DIR ".."

#define FILE_SIZE(NUM_OF_BLOCKS) NUM_OF_BLOCKS * BLOCK_SIZE 
#define DBLK_OFFSET(X) X*BLOCK_SIZE + main_block->d_start_blk
#define DBLK_NOFFSET(X) (X - main_block->d_start_blk)/BLOCK_SIZE
#define INIT_INDEX 0

char diskfile_path[PATH_MAX];

// Declare your in-memory data structures here

sblock_t main_block;

static long i_calls;
static long d_calls;
/* 
 * Get available inode number from bitmap
 */
int get_avail_ino() {
	char i_bitmap_s[BLOCK_SIZE];
	bitmap_t i_bitmap = (bitmap_t)i_bitmap_s;

	bio_read(main_block->i_bitmap_blk,i_bitmap);

	int i_num = ROOT_INO;
	while(get_bitmap(i_bitmap,i_num)){i_num++;}

	set_bitmap(i_bitmap, i_num);
	bio_write(main_block->i_bitmap_blk, i_bitmap);
	
	//i_calls++;
	//printf("iBlocks: %d\n", i_calls);
	return i_num;
}

/* 
 * Get available data block number from bitmap
 */
int get_avail_blkno() {
	char d_bitmap_s[BLOCK_SIZE];
	bitmap_t d_bitmap = (bitmap_t)d_bitmap_s;

	bio_read(main_block->d_bitmap_blk,d_bitmap);

	int d_num = 1;
	while(get_bitmap(d_bitmap,d_num)){d_num++;}

	set_bitmap(d_bitmap, d_num);
	bio_write(main_block->d_bitmap_blk, d_bitmap);
	
	///d_calls++;
	//printf("dBlocks: %d\n", d_calls); 
	return d_num;
}


int readi(uint16_t ino, struct inode *inode) {
	struct inode i_blk[NUM_INODES];
	memset(i_blk,!VALID, BLOCK_SIZE);
	uint32_t block_no = (ino*INODE_SIZE)/BLOCK_SIZE;
	uint32_t offset = (block_no*BLOCK_SIZE) + main_block->i_start_blk;
	int internal_offset = ino - (NUM_INODES*block_no);
	
	bio_read(offset,i_blk);

	if(!i_blk[internal_offset].valid){
		//perror("Read failed: Invalid inode");
		return -1;
	}

	memcpy(inode,&i_blk[internal_offset],INODE_SIZE);
	
	return 0;
}


int writei(uint16_t ino, struct inode *inode) {
	struct inode i_blk[NUM_INODES];
	memset(i_blk,!VALID, BLOCK_SIZE);
	uint32_t block_no = (ino*INODE_SIZE)/BLOCK_SIZE;
	uint32_t offset = (block_no*BLOCK_SIZE) + main_block->i_start_blk;
	int internal_offset = ino - (NUM_INODES*block_no);
	
	bio_read(offset,i_blk);

	memcpy(&i_blk[internal_offset], inode, INODE_SIZE);
	
	bio_write(offset,i_blk);

	return 0;
}


/* 
 * directory operations
 */
int dir_find(uint16_t ino, const char *fname, size_t name_len, struct dirent *dirent, int* disk_block_no) {
  // Step 3: Read directory's data block and check each directory entry.
  //If the name matches, then copy directory entry to dirent structure
	struct dirent curr_dirent[NUM_DIRENTS];
	memset(curr_dirent,!VALID, BLOCK_SIZE);
	struct inode curr_inode;
	int i;
	int j;
	// Step 1: Call readi() to get the inode using ino (inode number of current directory)
	if(readi(ino,&curr_inode) < 0){
		//perror("Could not read block");
		//free(curr_inode);
		return -1;
	}

	if(!curr_inode.valid){
		//perror("Inode not valid");
		//free(curr_inode);
		return -1;
	}
	// Step 2: Get data block of current directory from inode
	for(i = 0; i < NUM_DIRECT_PTR; i++){
		if(curr_inode.direct_ptr[i] != EMPTY_BLK){
			bio_read(curr_inode.direct_ptr[i],curr_dirent);
			for(j = 0; j < NUM_DIRENTS; j++){
				if(curr_dirent[j].valid == VALID){
					if(strcmp(curr_dirent[j].name, fname) == 0){
						memcpy(dirent, &curr_dirent[j], DIRENT_SIZE);
						*disk_block_no = i;
						return 0;
					}
				}
			}
		}
	}	
	//perror("Entry doesn't exist");
	return -1;
}

int dir_add(struct inode* dir_inode, uint16_t f_ino, const char *fname, size_t name_len) {
	struct dirent entry;
	int i;
	int j;
	int disk_block_no;

	if(dir_find(dir_inode->ino,fname,name_len,&entry,&disk_block_no) == 0){
			//perror("Directory Entry already Exists!");
			return -1;
	}

	for(i = 0; i < NUM_DIRECT_PTR; i++){
		if(dir_inode->direct_ptr[i] == EMPTY_BLK){
			dir_inode->direct_ptr[i] = DBLK_OFFSET(get_avail_blkno());
			dir_inode->size += DIRENT_SIZE;
			struct dirent n_entries[NUM_DIRENTS];
			bio_read(dir_inode->direct_ptr[i], n_entries);
			memset(n_entries,!VALID, BLOCK_SIZE);

			n_entries[INIT_INDEX].ino = f_ino;
			n_entries[INIT_INDEX].valid = VALID;
			memcpy(n_entries[INIT_INDEX].name, fname, name_len);
			n_entries[INIT_INDEX].len = name_len;

			bio_write(dir_inode->direct_ptr[i], n_entries);
			writei(dir_inode->ino, dir_inode);
			return 0;
		} 

		struct dirent entries[NUM_DIRENTS];
		memset(entries,!VALID, BLOCK_SIZE);
		bio_read(dir_inode->direct_ptr[i], entries);
		for(j = 0; j < NUM_DIRENTS; j++){
			if(entries[j].valid == !VALID){
				dir_inode->size += DIRENT_SIZE;

				entries[j].ino = f_ino;
				entries[j].valid = VALID;
				memcpy(entries[j].name, fname, name_len);
				entries[j].len = name_len;

				bio_write(dir_inode->direct_ptr[i], entries);
				return 0;
			}
		}
	}

	//perror("Could not add entry");
	return -1;
}

int dir_remove(struct inode* dir_inode, const char *fname, size_t name_len) {
	struct dirent entry;
	int j;
	int k;
	int disk_block_no;

	if(dir_find(dir_inode->ino,fname,name_len,&entry,&disk_block_no) < 0){
			//perror("Directory doesnt Exist!");
			return -1;
	}

	struct dirent entries[NUM_DIRENTS];
	memset(entries,!VALID, BLOCK_SIZE);
	bio_read(dir_inode->direct_ptr[disk_block_no], entries);
	for(j = 0; j < NUM_DIRENTS; j++){
		if(entries[j].valid == VALID){
			if(strcmp(entries[j].name, fname) == 0){
				memset(&entries[j],!VALID,DIRENT_SIZE);
				dir_inode->size -= DIRENT_SIZE;
				for(k = 0; k < NUM_DIRENTS; k++){
					if(entries[k].valid == VALID){
						bio_write(dir_inode->direct_ptr[disk_block_no], entries);
						return 0;
					}
				}
				char dbitmap[BLOCK_SIZE];
				int block_no = DBLK_NOFFSET(dir_inode->direct_ptr[disk_block_no]);
				bio_read(main_block->d_bitmap_blk, (bitmap_t)dbitmap);
				unset_bitmap((bitmap_t)dbitmap, block_no);
				bio_write(main_block->d_bitmap_blk, (bitmap_t)dbitmap);

				dir_inode->direct_ptr[disk_block_no] = EMPTY_BLK;
				//bio_write(dir_inode->direct_ptr[disk_block_no], entries);
				writei(dir_inode->ino,dir_inode);
				return 0;
			}
		}
	}
	//perror("Could not remove entry");
	return -1;
}

/* 
 * namei operation
 */
int get_node_by_path(const char *path, uint16_t ino, struct inode *inode) {
	
	// Step 1: Resolve the path name, walk through path, and finally, find its inode.
	// Note: You could either implement it in a iterative way or recursive way
	struct dirent curr_dir;
	int path_size = strlen(path)+1;
    char path_ar[path_size];
	int disk_block_no;

    memcpy(path_ar, path, path_size);

	if(strcmp(path,"/") == 0) {
		if(readi(ino, inode) < 0){
			//perror("Could not read block");
			return -1;
		}
		return 0;
	}
	
	char* fname = strtok(path_ar,"/");
	if(dir_find(ino,fname,strlen(fname),&curr_dir,&disk_block_no) < 0){
		//perror("Directory Entry Not Found");
		return -1;
	} else {
		if(readi(curr_dir.ino, inode) < 0){
			//perror("Could not read block");
			return -1;
		}
	}

	while(fname != NULL){
		fname = strtok(NULL,"/");
		if(fname == NULL){
			return 0;
		}
		if(dir_find(inode->ino,fname,strlen(fname),&curr_dir,&disk_block_no) < 0){
			//perror("Directory Entry Not Found");
			return -1;
		} else {
			if(readi(curr_dir.ino, inode) < 0){
				//perror("Could not read block");
				return -1;
			}
		}
	}

	return -1;
}

/* 
 * Make file system
 */
int tfs_mkfs() {

	// Call dev_init() to initialize (Create) Diskfile

	// write superblock information

	// initialize inode bitmap

	// initialize data block bitmap

	// update bitmap information for root directory

	// update inode for root directory

	struct inode root_node;
	main_block = (sblock_t) malloc(sizeof(sblock_t));

	char i_bitmap_s[BLOCK_SIZE];
	bitmap_t i_map = (bitmap_t)i_bitmap_s;
	memset(i_bitmap_s,!VALID, BLOCK_SIZE);

	dev_init(diskfile_path);
	main_block -> magic_num = MAGIC_NUM;
	main_block -> max_inum = MAX_INUM;
	main_block -> max_dnum = MAX_DNUM;
	main_block -> i_bitmap_blk = BLOCK_SIZE + START_ADDR;
	main_block -> d_bitmap_blk = main_block ->i_bitmap_blk + BLOCK_SIZE;
	main_block -> i_start_blk = main_block ->d_bitmap_blk + BLOCK_SIZE;
	main_block -> d_start_blk = main_block -> i_start_blk + (MAX_INUM * INODE_SIZE);

	bio_write(START_ADDR, main_block);

	set_bitmap(i_map, ROOT_INO);

   /* struct inode* root_inode = (struct inode*) calloc(1, sizeof(struct inode));*/
	/*init_inode(ROOT_INO, ROOT_INO, VALID, FILE_SIZE(0), S_IFDIR | S_IRWXU, 1, root_inode);*/
	MAKE_INODE(root_node, ROOT_INO, S_IFDIR);

	writei(ROOT_INO, &root_node);

	bio_write(main_block -> i_bitmap_blk, i_map);

	//free(i_map);
	//free(d_map);
	return 0;
}

/* 
 * FUSE file operations
 */
static void *tfs_init(struct fuse_conn_info *conn) {

	// Step 1a: If disk file is not found, call mkfs

  // Step 1b: If disk file is found, just initialize in-memory data structures
  // and read superblock from disk

	if(dev_open(diskfile_path) < 0){
		tfs_mkfs();
	} else{
		main_block = (sblock_t) malloc(sizeof(sblock_t));
		bio_read(START_ADDR, main_block);	
	}


	return NULL;
}

static void tfs_destroy(void *userdata) {

	// Step 1: De-allocate in-memory data structures

	// Step 2: Close diskfile

	free(main_block);
	main_block = NULL;
	
	dev_close();

}

static int tfs_getattr(const char *path, struct stat *stbuf) {

	// Step 1: call get_node_by_path() to get inode from path
	// Step 2: fill attribute of file into stbuf from inode
		struct inode i_buf;
		memset(stbuf, 0, sizeof(struct stat));
		
		if(get_node_by_path(path, ROOT_INO, &i_buf) < 0){
			perror("Could not find file");
			return -ENOENT;
		}
		stbuf->st_gid = getgid();
		stbuf->st_uid = getuid();
		
		if (i_buf.type == S_IFDIR){
			stbuf->st_mode = S_IFDIR | 0755;
			stbuf->st_nlink = 2;
		}
		else{
			stbuf->st_mode = S_IFREG | 0644;
			stbuf->st_nlink = 1;
		}
		
		stbuf->st_size =  i_buf.size;
		time(&stbuf->st_mtime);

		//free(i_buf);

	return 0;
}

static int tfs_opendir(const char *path, struct fuse_file_info *fi) {

	// Step 1: Call get_node_by_path() to get inode from path
	// Step 2: If not find, return -1
	struct inode i_buf;
	if(get_node_by_path(path, ROOT_INO,&i_buf) < 0) {
		perror("Could not find file");
		return -ENOENT;
	}
    return 0;
}

static int tfs_readdir(const char *path, void *buffer, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {

	// Step 1: Call get_node_by_path() to get inode from path
	struct inode i_buf;
	struct dirent entries[NUM_DIRENTS];
	int i;
	int j;
	if(get_node_by_path(path, ROOT_INO, &i_buf) < 0) {
		perror("Could not find file");
		//free(i_buf);
		return -ENOENT;
	}
	// Step 2: Read directory entries from its data blocks, and copy them to filler

	filler(buffer, CUR_DIR, NULL, 0);
    filler(buffer, PAR_DIR, NULL, 0);
	for(i = 0; i < NUM_DIRECT_PTR; i++){
		if(i_buf.direct_ptr[i] != EMPTY_BLK){
			bio_read(i_buf.direct_ptr[i], entries);
			for(j = 0; j < NUM_DIRENTS; j++){
				if(entries[j].valid == VALID){
					if(filler(buffer, entries[j].name, NULL, 0) != 0){
						//perror("We have an issue");
						return -ENOMEM;
					}
				}
			}
		}
	}
	return 0;
}


static int tfs_mkdir(const char *path, mode_t mode) {

	// Step 1: Use dirname() and basename() to separate parent directory path and target directory name
	char throw_away[strlen(path) + 1]; //because dirname and basename are stupid and change path
	strcpy(throw_away, path);
	char* dirnm = dirname(throw_away);
	// Step 2: Call get_node_by_path() to get inode of parent directory
	struct inode pdir_i;
	if(get_node_by_path(dirnm, ROOT_INO, &pdir_i) != 0) return -1;
	// Step 3: Call get_avail_ino() to get an available inode number
	uint16_t ino = get_avail_ino();
	// Step 4: Call dir_add() to add directory entry of target directory to parent directory	
	char* basenm = basename((char*)path);
	if(dir_add(&pdir_i, ino, basenm, strlen(basenm)) != 0) return -1;
	// Step 5: Update inode for target directory
	struct inode tdir_i;
	MAKE_INODE(tdir_i, ino, S_IFDIR);
	// Step 6: Call writei() to write inode to disk
	if(writei(ino, &tdir_i) != 0) return -1;

	return 0;
}

#define UNSET_BIT_BLK(BITMAP_START, T_NO)	\
	do{	\
		unsigned char BITMAP[BLOCK_SIZE];	\
		bio_read(BITMAP_START, BITMAP);	\
		unset_bitmap(BITMAP, T_NO);	\
		bio_write(BITMAP_START, BITMAP);	\
	}while(0);

static int tfs_rmdir(const char *path) {

	// Step 1: Use dirname() and basename() to separate parent directory path and target directory name
	char throw_away[strlen(path) + 1];
	strcpy(throw_away, path);
	char* dirnm = dirname(throw_away);
	// Step 2: Call get_node_by_path() to get inode of target directory
	struct inode pdir_i;
	if(get_node_by_path(dirnm, ROOT_INO, &pdir_i) != 0) return -1;

	char* basenm = basename((char*)path);
	struct dirent tdirent;
	int disk_block_no;
	if(dir_find(pdir_i.ino, basenm, strlen(basenm), &tdirent, &disk_block_no) != 0) return -1;
	
	//check for empty dir
	struct inode tdir_i;
	if(readi(tdirent.ino, &tdir_i) != 0) return -1;
	int i;
	for(i=0; i < NUM_DPTR(tdir_i); i++){
		if(tdir_i.direct_ptr[i] != -1){
			perror("Directory not empty");
			return -1;
		}
	}
	// Step 3: Clear data block bitmap of target directory
	// Step 4: Clear inode bitmap and its data block
	UNSET_BIT_BLK(main_block->i_bitmap_blk, tdirent.ino);	
	//didnt set blocks to zero because whats the point as long as bitmaps are right?
	// Step 6: Call dir_remove() to remove directory entry of target directory in its parent directory
	dir_remove(&pdir_i, basenm, strlen(basenm));

	return 0;
}

static int tfs_releasedir(const char *path, struct fuse_file_info *fi) {
	// For this project, you don't need to fill this function
	// But DO NOT DELETE IT!
    return 0;
}

static int tfs_create(const char *path, mode_t mode, struct fuse_file_info *fi) {

	// Step 1: Use dirname() and basename() to separate parent directory path and target file name
	char throw_away[strlen(path) + 1];
	strcpy(throw_away, path);
	char* dirnm = dirname(throw_away);
	// Step 2: Call get_node_by_path() to get inode of parent directory
	struct inode pdir_i;
	if(get_node_by_path(dirnm, ROOT_INO, &pdir_i) != 0) return -1;
	// Step 3: Call get_avail_ino() to get an available inode number
	uint16_t t_ino = get_avail_ino();
	char* basenm = basename((char*) path);
	// Step 4: Call dir_add() to add directory entry of target file to parent directory
	if(dir_add(&pdir_i, t_ino, basenm, strlen(basenm)) != 0) return -1;
	// Step 5: Update inode for target file
	struct inode t_inode;
	MAKE_INODE(t_inode, t_ino, S_IFREG);
	// Step 6: Call writei() to write inode to disk
	if(writei(t_ino, &t_inode) != 0) return -1;

	return 0;
}

static int tfs_open(const char *path, struct fuse_file_info *fi) {

	// Step 1: Call get_node_by_path() to get inode from path
	struct inode tinode;
	if(get_node_by_path(path, ROOT_INO, &tinode) != 0) return -1;
	// Step 2: If not find, return -1

	return 0;
}

static int tfs_read(const char *path, char *buffer, size_t size, off_t offset, struct fuse_file_info *fi) {

	// Step 1: You could call get_node_by_path() to get inode from path
	struct inode t_inode;
	if(get_node_by_path(path, ROOT_INO, &t_inode) != 0) return -1;
	// Step 2: Based on size and offset, read its data blocks from disk
	char r_buf[BLOCK_SIZE];
	char* cpy_buf = (char*) malloc(sizeof(char)*size);
	memset(r_buf, 0, BLOCK_SIZE);
	memset(cpy_buf, 0, size);
	
	int start_block = offset/BLOCK_SIZE;
	offset -= (start_block * BLOCK_SIZE);
	// Step 3: copy the correct amount of data from offset to buffer
	int i;
	int k = 0;
	for(i=start_block; i < NUM_DPTR(t_inode) && size != 0; i++){
		if(t_inode.direct_ptr[i] == -1) break;	//file data blocks shouldnt have skips :/
		if(bio_read(t_inode.direct_ptr[i], r_buf) < 0 ) return k;
		if(BLOCK_SIZE - offset >= size){
			memcpy(cpy_buf + k, r_buf+offset, size);
			offset = 0;	//offset no longer needed for next block
			k += size;
			size -= size;
		}else{
			memcpy(cpy_buf+k, r_buf+offset, BLOCK_SIZE - offset);
			k += (BLOCK_SIZE - offset);
			offset = 0;
			size -= (BLOCK_SIZE - offset);
		}
	}
	memcpy(buffer, cpy_buf, k);
	free(cpy_buf);
	// Note: this function should return the amount of bytes you copied to buffer
	return k;
}

static int tfs_write(const char *path, const char *buffer, size_t size, off_t offset, struct fuse_file_info *fi) {
	// Step 1: You could call get_node_by_path() to get inode from path
	struct inode t_inode;
	if(get_node_by_path(path, ROOT_INO, &t_inode) != 0) return -1;
	// Step 2: Based on size and offset, read its data blocks from disk
	char r_buf[BLOCK_SIZE];
	memset(r_buf, 0, BLOCK_SIZE);

	int start_block = offset/BLOCK_SIZE;
	offset -= (start_block * BLOCK_SIZE);
	// Step 3: Write the correct amount of data from offset to disk
	int i;
	int k=0;
	for(i = start_block; i < NUM_DPTR(t_inode) && size != 0; i++){
		if(t_inode.direct_ptr[i] == -1){
			if(size == 0) break;
			t_inode.direct_ptr[i] = DBLK_OFFSET(get_avail_blkno());
		}
		if(bio_read(t_inode.direct_ptr[i], r_buf) < 0) return k;
		if(BLOCK_SIZE - offset >= size){
			memcpy(r_buf + offset, buffer + k, size);
			if(bio_write(t_inode.direct_ptr[i], r_buf) < 0 ) return k;
			k += size;
			size -= size;
			offset = 0;
		}else{
			memcpy(r_buf + offset, buffer + k, BLOCK_SIZE - offset);
			if(bio_write(t_inode.direct_ptr[i], r_buf) < 0) return k;
			k += (BLOCK_SIZE - offset);
			size -= (BLOCK_SIZE - offset);
			offset = 0;
		}
	}
	// Step 4: Update the inode info and write it to disk
	t_inode.size += k;
	if(writei(t_inode.ino, &t_inode) < 0) return -1;
	// Note: this function should return the amount of bytes you write to disk
	return k;
}

static int tfs_unlink(const char *path) {

	// Step 1: Use dirname() and basename() to separate parent directory path and target file name
	char throw_away[strlen(path)+1];
	strcpy(throw_away, path);
	char* dirnm = dirname(throw_away);

	struct inode pdir_i;
	if(get_node_by_path(dirnm, ROOT_INO, &pdir_i) != 0) return -1;
	// Step 2: Call get_node_by_path() to get inode of target file
	char* basenm = basename((char*) path);
	struct dirent tdirent;
	int disk_block_no;
	if(dir_find(pdir_i.ino, basenm, strlen(basenm), &tdirent,&disk_block_no) != 0) return -1;
	// Step 3: Clear data block bitmap of target file
	unsigned char d_bitmap[BLOCK_SIZE];
	if(bio_read(main_block->d_bitmap_blk, d_bitmap) < 0) return -1;
	// Step 4: Clear inode bitmap and its data block	
	UNSET_BIT_BLK(main_block->i_bitmap_blk, tdirent.ino);
	
	//unset all direct_ptr data blocks in bitmap
	struct inode t_inode;
	if(readi(tdirent.ino, &t_inode) < 0) return -1;
	//ideally this part could be optimized by having a bitmap indicating unsets
	//and then bitwise AND with actual bitmap to unset multiple bits
	int i;
	for(i=0; i < NUM_DPTR(t_inode); i++){
		if(t_inode.direct_ptr[i] != -1){
			unset_bitmap(d_bitmap, DBLK_NOFFSET(t_inode.direct_ptr[i]));
		}
	}
	if(bio_write(main_block->d_bitmap_blk, d_bitmap) < 0) return -1;
	// Step 5: Call get_node_by_path() to get inode of parent directory
	// Step 6: Call dir_remove() to remove directory entry of target file in its parent directory
	if(dir_remove(&pdir_i, basenm, strlen(basenm)) < 0) return -1;
	return 0;
}

static int tfs_truncate(const char *path, off_t size) {
	// For this project, you don't need to fill this function
	// But DO NOT DELETE IT!
    return 0;
}

static int tfs_release(const char *path, struct fuse_file_info *fi) {
	// For this project, you don't need to fill this function
	// But DO NOT DELETE IT!
	return 0;
}

static int tfs_flush(const char * path, struct fuse_file_info * fi) {
	// For this project, you don't need to fill this function
	// But DO NOT DELETE IT!
    return 0;
}

static int tfs_utimens(const char *path, const struct timespec tv[2]) {
	// For this project, you don't need to fill this function
	// But DO NOT DELETE IT!
    return 0;
}


static struct fuse_operations tfs_ope = {
	.init		= tfs_init,
	.destroy	= tfs_destroy,

	.getattr	= tfs_getattr,
	.readdir	= tfs_readdir,
	.opendir	= tfs_opendir,
	.releasedir	= tfs_releasedir,
	.mkdir		= tfs_mkdir,
	.rmdir		= tfs_rmdir,

	.create		= tfs_create,
	.open		= tfs_open,
	.read 		= tfs_read,
	.write		= tfs_write,
	.unlink		= tfs_unlink,

	.truncate   = tfs_truncate,
	.flush      = tfs_flush,
	.utimens    = tfs_utimens,
	.release	= tfs_release
};


int main(int argc, char *argv[]) {
	int fuse_stat;
	//struct inode* i_buf = (struct inode*) malloc(sizeof(struct inode));
	getcwd(diskfile_path, PATH_MAX);
	strcat(diskfile_path, "/DISKFILE");

	fuse_stat = fuse_main(argc, argv, &tfs_ope, NULL);

	return fuse_stat;
	//return 0;
}

