/*
 *  Copyright (C) 2020 CS416 Rutgers CS
 *	Tiny File System
 *
 *	File:	tfs.h
 *
 */

#include <linux/limits.h>
#include <sys/stat.h>
#include <unistd.h>

#ifndef _TFS_H
#define _TFS_H

#define MAGIC_NUM 0x5C3A
#define MAX_INUM 1024
#define MAX_DNUM 16384
#define START_ADDR 0
#define ROOT_INO 2
#define NUM_DPTR(INODE) sizeof(INODE.direct_ptr)/sizeof(int)
#define NUM_DIRECT_PTR 16
#define VALID 1
#define EMPTY_BLK -1

#define MAKE_INODE(INODE, INO, TYPE)	\
	do {	\
		INODE.ino = INO;	\
		INODE.valid = 1;	\
		INODE.type = TYPE;	\
		INODE.size = 0;		\
		memset(INODE.direct_ptr, -1, sizeof(INODE.direct_ptr));	\
	}while(0);

struct superblock {
	uint32_t	magic_num;			/* magic number */
	uint16_t	max_inum;			/* maximum inode number */
	uint16_t	max_dnum;			/* maximum data block number */
	uint32_t	i_bitmap_blk;		/* start block of inode bitmap */
	uint32_t	d_bitmap_blk;		/* start block of data block bitmap */
	uint32_t	i_start_blk;		/* start block of inode region */
	uint32_t	d_start_blk;		/* start block of data block region */
};

struct inode {
	uint16_t	ino;				/* inode number */
	uint16_t	valid;				/* validity of the inode */
	uint32_t	size;				/* size of the file */
	uint32_t	type;				/* type of the file */
	uint32_t	link;				/* link count */
	int			direct_ptr[16];		/* direct pointer to data block */
	int			indirect_ptr[8];	/* indirect pointer to data block */
	struct stat	vstat;				/* inode stat */
};

struct dirent {
	uint16_t ino;					/* inode number of the directory entry */
	uint16_t valid;					/* validity of the directory entry */
	char name[250];					/* name of the directory entry */
	uint16_t len;					/* length of name */
};


/*
 * bitmap operations
 */
typedef unsigned char* bitmap_t;
typedef struct superblock* sblock_t;

void set_bitmap(bitmap_t b, int i) {
    b[i / 8] |= 1 << (i & 7);
}

void unset_bitmap(bitmap_t b, int i) {
    b[i / 8] &= ~(1 << (i & 7));
}

uint8_t get_bitmap(bitmap_t b, int i) {
    return b[i / 8] & (1 << (i & 7)) ? 1 : 0;
}

#endif
